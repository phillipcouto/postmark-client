# Changelog

This is the log of changes to the crate.

## UNRELEASED CHANAGES

## v0.3.0

- Updated dependencies

## v0.2.1

- Updated comments on bounced structures
- Fixed the deserialization of `message_id` in EmailReponse

## v0.2.0

- Simplified EmailBody enum
- Hid emails module
- Added bounce API methods

## v0.1.0

- Initial alpha release with support for sending emails and templated emails

