//! # Postmark Client
//!
//! Postmark client is a client to interact with postmark which used reqwest
//! for issuing API calls.
//!
//! The crate aims to be feature complete against the full Postmark API but
//! until 1.0 it is a work in progress.
//!
//! To use the client create a new client and then use it to execute the
//! desired postmark operations.
//!
//! <https://postmarkapp.com/developer/api/overview>

mod emails;

pub mod bounce;
pub mod client;
pub mod errors;
pub mod types;

pub use crate::client::Client;

type Result<T> = std::result::Result<T, errors::PostmarkClientError>;
