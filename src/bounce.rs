use crate::{Client, Result};
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct DeliveryStat {
    pub name: String,
    #[serde(rename = "Type")]
    pub bounce_type: Option<String>,
    pub count: i64,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct DeliveryStatsResponse {
    /// Number of inactive emails
    pub inactive_mails: i64,
    /// List of [bounce types](https://postmarkapp.com/developer/api/bounce-api#bounce-types) with total counts.
    pub bounces: Vec<DeliveryStat>,
}

#[derive(Debug, Serialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct BouncesQueryParamaters {
    /// Number of bounces to return per request. Max 500. Count + Offset cannot exceed 10,000 bounces.
    pub count: u16,
    /// Number of bounces to skip. Count + Offset cannot exceed 10,000 bounces.
    pub offset: u16,
    /// Filter by [type of bounce](https://postmarkapp.com/developer/api/bounce-api#bounce-types)
    #[serde(rename = "type")]
    pub bounce_type: Option<String>,
    /// Filter by emails that were deactivated by Postmark due to the bounce. Set to true or false.
    /// If this isn’t specified it will return both active and inactive.
    pub inactive: Option<bool>,
    /// Filter by email address
    pub email_filter: Option<String>,
    /// Filter by tag
    pub tag: Option<String>,
    /// Filter by messageID
    pub message_id: Option<String>,
    /// Filter messages starting from the date/time specified (inclusive). e.g. 2021-01-01T12:00:00.
    /// Our API uses Eastern Time Zone.
    #[serde(rename = "fromdate")]
    pub from_date: Option<String>,
    /// Filter messages up to the date/time specified (inclusive). e.g. 2021-01-01T12:00:00.
    /// Our API uses Eastern Time Zone.
    #[serde(rename = "todate")]
    pub to_date: Option<String>,
    /// Filter by message stream ID. If not provided, message will default to the outbound transactional stream.
    pub message_stream: Option<String>,
}

impl BouncesQueryParamaters {
    /// Creates a new BouncesQueryParameters requiring the minimum parameters
    pub fn new(count: u16, offset: u16) -> BouncesQueryParamaters {
        BouncesQueryParamaters {
            count,
            offset,
            ..Default::default()
        }
    }
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct BouncedEmail {
    /// ID of bounce
    #[serde(rename = "ID")]
    pub id: String,
    /// [Bounce type](https://postmarkapp.com/developer/api/bounce-api#bounce-types)
    #[serde(rename = "Type")]
    pub bounce_type: String,
    /// [Bounce type code](https://postmarkapp.com/developer/api/bounce-api#bounce-types)
    pub type_code: String,
    /// [Bounce type name](https://postmarkapp.com/developer/api/bounce-api#bounce-types)
    pub name: String,
    /// Tag name
    pub tag: String,
    /// ID of message
    #[serde(rename = "MessageID")]
    pub message_id: String,
    /// ID of server that sent the message
    #[serde(rename = "ServerID")]
    pub server_id: String,
    /// The outbound sending message stream for the message.
    pub message_stream: String,
    /// Description of bounce
    pub description: String,
    /// Details on the bounce
    pub details: String,
    /// Email address that bounced
    pub email: String,
    /// Original sender of the bounced message, if available. For example, spam
    /// complaints do not include the original sender address.
    pub from: String,
    /// Timestamp of bounce
    pub bounced_at: String,
    /// Specifies whether or not you can get a [raw dump](https://postmarkapp.com/developer/api/bounce-api#bounce-dump)
    /// from this bounce. Postmark doesn’t store bounce dumps older than 30 days.
    pub dump_available: bool,
    /// Specifies if the bounce caused Postmark to deactivate this email.
    pub inactive: bool,
    /// Specifies whether or not you are able to reactivate this email.
    pub can_activate: bool,
    /// Email subject
    pub subject: String,
    /// Bounce content
    pub content: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct BouncedEmailsReponse {
    /// Number of records returned
    pub total_count: u16,
    /// List of individual bounces
    pub bounces: Vec<BouncedEmail>,
}
#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct BounceDumpResponse {
    /// Raw source of bounce. If no dump is available this will return an empty string.
    pub body: String,
}
#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct ActivateBounceResponse {
    /// Response message
    pub message: String,
    /// Bounce details
    pub bounce: BouncedEmail,
}

impl Client {
    /// Lists all the bounce stats for the server the token is associated with.
    ///
    /// <https://postmarkapp.com/developer/api/bounce-api#delivery-stats>
    pub async fn get_delivery_stats(&self) -> Result<DeliveryStatsResponse> {
        self.get("/deliverystats").await
    }
    /// The bounces search allows you to return up-to 10,000 bounces in a search.
    /// For searches where you're looking to retrieve more than 10,000 bounces use
    /// parameters like todate and fromdate to filter the messages.
    ///
    /// <https://postmarkapp.com/developer/api/bounce-api#bounces>
    pub async fn get_bounces(
        &self,
        query: &BouncesQueryParamaters,
    ) -> Result<BouncedEmailsReponse> {
        self.get_with_query("/bounces", query).await
    }
    /// Gets a single bounced email using the provided ID.
    ///
    /// <https://postmarkapp.com/developer/api/bounce-api#single-bounce>
    pub async fn get_bounce(&self, bounce_id: &str) -> Result<BouncedEmail> {
        self.get(format!("/bounces/{:}", bounce_id).as_str()).await
    }
    /// Gets the SMTP dump for the bounced email.
    ///
    /// <https://postmarkapp.com/developer/api/bounce-api#bounce-dump>
    pub async fn get_bounce_dump(&self, bounce_id: &str) -> Result<BounceDumpResponse> {
        self.get(format!("/bounces/{:}/dump", bounce_id).as_str())
            .await
    }
    /// Reactivates thes email address in the bounced email
    ///
    /// <https://postmarkapp.com/developer/api/bounce-api#activate-bounce>
    pub async fn activate_bounce(&self, bounce_id: &str) -> Result<ActivateBounceResponse> {
        self.put(format!("/bounces/{:}/activate", bounce_id).as_str())
            .await
    }
}
