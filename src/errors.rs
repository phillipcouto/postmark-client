use reqwest::StatusCode;
use thiserror::Error;

use crate::types::APIError;

/// Represents the types of errors that can be encountered
#[derive(Error, Debug)]
pub enum PostmarkClientError {
    /// Postmark returned an unauthorized status
    ///
    /// <https://postmarkapp.com/developer/api/overview#response-codes>
    #[error("unauthorized")]
    Unauthorized,
    /// Postmark returned a request to large status
    ///
    /// <https://postmarkapp.com/developer/api/overview#response-codes>
    #[error("request exceeded postmarks size limit")]
    RequestToLarge,
    /// Postmark returned an unprossable entity status
    ///
    /// <https://postmarkapp.com/developer/api/overview#response-codes>
    #[error("postmark was unable to process the request")]
    UnprocessableEntity(APIError),
    /// Postmark returned a rate limit exceeded status
    ///
    /// <https://postmarkapp.com/developer/api/overview#response-codes>
    #[error("rate limit exceeded")]
    RateLimitExceeded,
    /// Postmark returned an internal server error status
    ///
    /// <https://postmarkapp.com/developer/api/overview#response-codes>
    #[error("internal postmark server error")]
    InternalServerError,
    /// Postmark returned a service unavailable status
    ///
    /// <https://postmarkapp.com/developer/api/overview#response-codes>
    #[error("postmark is currently unavailable")]
    ServiceUnavailable,
    /// Error from reqwest
    #[error(transparent)]
    Reqwest(#[from] reqwest::Error),
    /// Postmark returned an unknown status
    ///
    /// <https://postmarkapp.com/developer/api/overview#response-codes>
    #[error("unknown postmark status {0}")]
    UnknownPostmarkStatus(StatusCode),
}
