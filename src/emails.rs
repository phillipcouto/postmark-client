use serde::Serialize;

use crate::{
    types::{Email, EmailResponse, TemplatedEmail},
    Client, Result,
};

impl Client {
    /// Sends a batch of Emails in a single request, each email in the batch has a corresponding
    /// EmailResponse in the result of this function.
    ///
    /// <https://postmarkapp.com/developer/api/email-api#send-batch-emails>
    pub async fn send_batch_emails(&self, emails: &[Email]) -> Result<Vec<EmailResponse>> {
        self.post("/email/batch", emails).await
    }
    /// Sends a batch of template emails in a single request, each email in the batch has a
    /// corresponding EmailResoinse in the result of this function.
    ///
    /// <https://postmarkapp.com/developer/api/templates-api#send-batch-with-templates>
    pub async fn send_batch_template_emails<M>(
        &self,
        email: &[TemplatedEmail<M>],
    ) -> Result<EmailResponse>
    where
        M: Serialize,
    {
        //TODO: Fix this function to support emails with different types of models
        self.post("/email/batchWithTemplates", email).await
    }
    /// Sends an email.
    ///
    /// <https://postmarkapp.com/developer/api/email-api#send-a-single-email>
    pub async fn send_email(&self, email: &Email) -> Result<EmailResponse> {
        self.post("/email", email).await
    }
    /// Sends a template email.
    ///
    /// <https://postmarkapp.com/developer/api/templates-api#email-with-template>
    pub async fn send_template_email<M>(&self, email: &TemplatedEmail<M>) -> Result<EmailResponse>
    where
        M: Serialize,
    {
        self.post("/email/withTemplate", email).await
    }
}
