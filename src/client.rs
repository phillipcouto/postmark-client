use reqwest::{Response, StatusCode};
use serde::de::DeserializeOwned;
use serde::Serialize;

use crate::errors::PostmarkClientError;
use crate::types::APIError;
use crate::{errors, Result};

const POSTMARK_API_URL_BASE: &str = "https://api.postmarkapp.com";
const POSTMARK_TOKEN_HEADER: &str = "X-Postmark-Server-Token";

/// Client exposes the Postmark functionality to be easily used
/// within rust applications.
///
/// The client can create its own internal reqwest::Client or
/// one can be provided if there are specific configurations
/// desired by the application.
pub struct Client {
    http_client: reqwest::Client,
    token: String,
}

impl Client {
    /// Create a new postmark client with the provided token
    pub fn new(token: String) -> Client {
        Client {
            http_client: reqwest::Client::new(),
            token,
        }
    }
    /// Create a new postmark client with the provided reqwest client and token
    pub fn new_with_client(token: String, http_client: reqwest::Client) -> Client {
        Client { http_client, token }
    }
    /// Extracts the error based on the status code from postmark when the code
    /// is not a successful status code.
    ///
    /// Based on <https://postmarkapp.com/developer/api/overview#response-codes>
    async fn extract_error(response: Response) -> errors::PostmarkClientError {
        let status = response.status();
        match status {
            StatusCode::UNAUTHORIZED => PostmarkClientError::Unauthorized,
            StatusCode::NOT_FOUND => PostmarkClientError::RequestToLarge,
            StatusCode::UNPROCESSABLE_ENTITY => {
                let data = response.json::<APIError>().await;
                match data {
                    Err(e) => PostmarkClientError::Reqwest(e),
                    Ok(e) => PostmarkClientError::UnprocessableEntity(e),
                }
            }
            StatusCode::TOO_MANY_REQUESTS => PostmarkClientError::RateLimitExceeded,
            StatusCode::INTERNAL_SERVER_ERROR => PostmarkClientError::InternalServerError,
            StatusCode::SERVICE_UNAVAILABLE => PostmarkClientError::ServiceUnavailable,
            _ => PostmarkClientError::UnknownPostmarkStatus(status),
        }
    }

    pub(crate) async fn get<R>(&self, path: &str) -> Result<R>
    where
        R: DeserializeOwned,
    {
        let response = self
            .http_client
            .get(format!("{:}{:}", POSTMARK_API_URL_BASE, path))
            .header(POSTMARK_TOKEN_HEADER, &self.token)
            .send()
            .await?;

        if !response.status().is_success() {
            return Err(Client::extract_error(response).await);
        }

        Ok(response.json::<R>().await?)
    }

    pub(crate) async fn get_with_query<Q, R>(&self, path: &str, query: &Q) -> Result<R>
    where
        Q: Serialize + ?Sized,
        R: DeserializeOwned,
    {
        let response = self
            .http_client
            .get(format!("{:}{:}", POSTMARK_API_URL_BASE, path))
            .header(POSTMARK_TOKEN_HEADER, &self.token)
            .query(query)
            .send()
            .await?;

        if !response.status().is_success() {
            return Err(Client::extract_error(response).await);
        }

        Ok(response.json::<R>().await?)
    }

    pub(crate) async fn post<B, R>(&self, path: &str, body: &B) -> Result<R>
    where
        B: Serialize + ?Sized,
        R: DeserializeOwned,
    {
        let response = self
            .http_client
            .post(format!("{:}{:}", POSTMARK_API_URL_BASE, path))
            .header(POSTMARK_TOKEN_HEADER, &self.token)
            .json(body)
            .send()
            .await?;

        if !response.status().is_success() {
            return Err(Client::extract_error(response).await);
        }

        Ok(response.json::<R>().await?)
    }

    pub(crate) async fn put<R>(&self, path: &str) -> Result<R>
    where
        R: DeserializeOwned,
    {
        let response = self
            .http_client
            .post(format!("{:}{:}", POSTMARK_API_URL_BASE, path))
            .header(POSTMARK_TOKEN_HEADER, &self.token)
            .send()
            .await?;

        if !response.status().is_success() {
            return Err(Client::extract_error(response).await);
        }

        Ok(response.json::<R>().await?)
    }
}
