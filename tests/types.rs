use std::collections::HashMap;

use postmark_client::types::{Email, EmailBody, TemplateToUse, TemplatedEmail};

#[test]
fn test_email_serialize() -> Result<(), serde_json::Error> {
    let email = Email::new(
        "from@test.com".to_string(),
        "to@test.com".to_string(),
        EmailBody::Text("Hello World".to_string()),
    );

    let encoded = serde_json::to_string(&email)?;
    let expecting =
        "{\"From\":\"from@test.com\",\"To\":\"to@test.com\",\"TextBody\":\"Hello World\"}";

    assert_eq!(encoded, expecting);
    Ok(())
}

#[test]
fn test_templated_email_serialize() -> Result<(), serde_json::Error> {
    let mut model: HashMap<String, String> = HashMap::new();
    model.insert("key".to_string(), "value".to_string());

    let email = TemplatedEmail::new(
        "from@test.com".to_string(),
        "to@test.com".to_string(),
        TemplateToUse::Alias("test-email".to_string()),
        model,
    );

    let encoded = serde_json::to_string(&email)?;
    let expecting =
        "{\"TemplateAlias\":\"test-email\",\"TemplateModel\":{\"key\":\"value\"},\"From\":\"from@test.com\",\"To\":\"to@test.com\"}";

    assert_eq!(encoded, expecting);
    Ok(())
}
